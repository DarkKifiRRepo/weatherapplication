//
//  ModelToPresent.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 19/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

protocol ModelToPresent: class {
    var country: String { get set }
    var weatherIcon: UIImage { get set }
    var pressure: String { get set }
    var humidity: String { get set }
    var temperature: String { get set }
    var apparentTemperature: String { get set }
    var model: Box<CurrentWeather?> { get }
}

extension ModelToPresent {
    
    static func initSelf(completionHandler: @escaping (ModelToPresent)->Void) {
        let _ = CurrentWeather.updateModel { (currentWeather) in
            completionHandler((currentWeather as! CurrentWeather).readyData(currentWeather: currentWeather)!)
        }
    }
}

class ViewModel: ModelToPresent {
    var country: String
    var weatherIcon: UIImage
    var pressure: String
    var humidity: String
    var temperature: String
    var apparentTemperature: String
    var model: Box<CurrentWeather?>
    
    init(country: String, icon: UIImage, pressure: String, humidity: String, temperature: String, apparentTemperature: String, bind: CurrentWeather?) {
        self.country = country
        self.pressure = pressure
        self.humidity = humidity
        self.temperature = temperature
        self.apparentTemperature = apparentTemperature
        self.weatherIcon = icon
        self.model = Box(bind)
    }
}
