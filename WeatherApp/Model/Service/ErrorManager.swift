//
//  ErrorManager.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 19/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

public let EVSNetworkingErrorDomain = "ru.evseev.WeatherApp.NetworkingError"
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200
