//
//  ParticlesView.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 22/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit
import SpriteKit

class ParticlesView: SKView, ParticleSceneGenerator {
    
    var viewModel: Box<WeatherIcons?> = Box(nil)
    
    override func didMoveToSuperview() {
        allowsTransparency = true
        backgroundColor = UIColor.clear
        
        defaultScene()
        
        viewModel.bind { [unowned self] in
            guard let newValue = $0 else { return }
            DispatchQueue.main.async {
                let scene = SKScene(size: self.bounds.size)
                scene.backgroundColor = UIColor.clear
                let particles = self.generate(with: newValue, toView: self)
                for particle in particles {
                    scene.addChild(particle!)
                }
                scene.backgroundColor = .clear
                self.presentScene(scene)
            }
        }
    }
}

extension ParticlesView {
    private func defaultScene() {
        let scene = SKScene(size: self.bounds.size)
        scene.backgroundColor = UIColor.clear
        presentScene(scene)
    }
}
