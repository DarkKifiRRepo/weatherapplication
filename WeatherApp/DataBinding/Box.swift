//
//  Box.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 21/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

// DataBinding - Boxing
class Box<T> {
    typealias Listener = (T) -> ()
    
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    func bind(listener: @escaping Listener) {
        self.listener = listener
        listener(value)
    }
    
    init(_ value: T) {
        self.value = value
    }
}
