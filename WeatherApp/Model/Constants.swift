//
//  Constants.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 20/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit
    // apiKey aeec351988182feb13d2fea3161e9310
    // latitude: 55.750777, longitude: 37.714217 - Moscow
struct Constants {
    let apiKey: String = "aeec351988182feb13d2fea3161e9310"
    let coordinates: Coordinates = Coordinates(latitude: 55.750777, longitude: 37.714217)
}

struct Colors {
    let deepBlue: UIColor = UIColor(red:0.09, green:0.34, blue:0.95, alpha:1.0)
    let lightBlue: UIColor = UIColor(red:0.50, green:0.96, blue:0.96, alpha:1.0)
    let blue: UIColor = UIColor.blue
    let gray: UIColor = UIColor(red:0.56, green:0.54, blue:0.54, alpha:1.0)
}
