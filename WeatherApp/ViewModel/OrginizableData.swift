//
//  OrginizeData.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 19/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol OrginizableData: ConvertableUnits { }

extension OrginizableData {
    func readyData(currentWeather: CurrentWeatherModel?) -> ModelToPresent? {
        let readyCurrentWeather = self.convertToRU(currentData: currentWeather)
        if let readyCurrentWeather = readyCurrentWeather {
            let viewData = ViewModel(
                country: readyCurrentWeather.country,
                icon: readyCurrentWeather.weatherIcon.image,
                pressure: "\(String(Int(readyCurrentWeather.pressure))) mm",
                humidity: "\(String(Int(readyCurrentWeather.humidity))) %",
                temperature: "\(String(Int(readyCurrentWeather.temperature))) ˚C",
                apparentTemperature: "Feels like: \(String(Int(readyCurrentWeather.apparentTemperature))) ˚C",
                bind: readyCurrentWeather as? CurrentWeather)
            return viewData
        } else { return nil }
    }
}
