//
//  LocationDetermination.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 26/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    static let instance = LocationManager()
    private let manager = CLLocationManager()
    lazy var authorizationStatus: CLAuthorizationStatus = {
        return CLLocationManager.authorizationStatus()
    }()
    var userPlace: String?
    var coordinates: Coordinates = Constants().coordinates {
        didSet {
            print("user location was changed to: \(coordinates.latitude), \(coordinates.longitude)")
            manager.stopUpdatingLocation()
        }
    }
}

extension LocationManager {
    func defineLocation () {
        manager.requestWhenInUseAuthorization()
        if authorizationStatus != .authorizedWhenInUse { return }
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyKilometer
        manager.distanceFilter = 5
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        coordinates = Coordinates(latitude: locations.last!.coordinate.latitude, longitude: locations.last!.coordinate.longitude)
        let coding = CLGeocoder()
        if let location = locations.last {
            coding.reverseGeocodeLocation(location) { [unowned self] (placemark, error) in
                if let place = placemark {
                        self.userPlace = ((place.first?.country)! + ", " + (place.first?.locality)!)
                }
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userLocationUpdated"), object: nil)
    }
}
