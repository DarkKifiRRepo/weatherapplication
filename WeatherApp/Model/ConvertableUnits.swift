//
//  ConvertableUnits.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 20/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol ConvertableUnits { }

extension ConvertableUnits {
    func convertToRU(currentData: CurrentWeatherModel?) -> CurrentWeatherModel? {
        if let currentData = currentData {
            currentData.temperature = (currentData.temperature  - 32) * 5 / 9
            currentData.apparentTemperature = (currentData.apparentTemperature  - 32) * 5 / 9
            currentData.humidity = currentData.humidity  * 100
            currentData.pressure = currentData.pressure  * 0.750
            return currentData
        } else { return nil }
    }
}
