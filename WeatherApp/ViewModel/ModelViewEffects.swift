//
//  ModelViewEffects.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 24/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

protocol ModelViewEffects {
    var currentWeather: CurrentWeather? { get set }
}

extension ModelViewEffects {
    static func updateModel(currentWeatherType weather: CurrentWeatherModel?) -> ModelViewEffects {
        return ViewEffects(currentWeather: weather as! CurrentWeather)
    }
}

class ViewEffects: ModelViewEffects {
    var currentWeather: CurrentWeather?
    
    init(currentWeather: CurrentWeather) {
        self.currentWeather = currentWeather
    }
}
