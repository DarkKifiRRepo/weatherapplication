//
//  ViewController.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 19/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import UIKit
import SpriteKit

class MainViewController: UIViewController {
    
    @IBOutlet private weak var countryLabel: UILabel!
    @IBOutlet private weak var weatherIcon: UIImageView!
    @IBOutlet private weak var pressureLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var humidityLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var apparentTemperatureLabel: UILabel!
    @IBOutlet weak var upperContentView: UIView!
    @IBOutlet weak var bottomContentView: UIView!
    @IBOutlet private weak var particlesView: ParticlesView!
    @IBOutlet private weak var refreshButton: UIButton!
    
    private var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            gradientLayer.colors = [Colors().blue.cgColor, Colors().deepBlue.cgColor, Colors().lightBlue.cgColor]
            gradientLayer.locations = [0, 0.1, 1]
            gradientLayer.frame = view.bounds
        }
    }
    
    var viewModel: Box<ModelToPresent?> = Box(nil)
    private var timer = Timer()
    

    
    
    @IBAction func refreshButtonPressed(_ sender: UIButton) {
        sender.isEnabled = false
        self.activityIndicator.startAnimating()
        sender.rotate360Degrees()
        self.startTimer()
        ViewModel.initSelf { [unowned self] (result) in
            DispatchQueue.main.async {
                self.viewModel.value = result
                self.timer.invalidate()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        upperContentView.layer.cornerRadius = 25
        bottomContentView.layer.cornerRadius = 15
        
        gradientLayer = CAGradientLayer()
        view.layer.insertSublayer(gradientLayer, at: 0)
        
        startTimer()
        activityIndicator.startAnimating()
        refreshButton.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: NSNotification.Name("userLocationUpdated"), object: nil)
        
        let authorizationStatus = LocationManager.instance.authorizationStatus
        if authorizationStatus != .authorizedWhenInUse {
            alertDeniedAccess()
            timer.invalidate()
            return
        }
    }
}

extension MainViewController {
    
    private func modelsBinding() {
        viewModel.bind { [unowned self] in
            guard let newValue = $0 else { return }
            
            DispatchQueue.main.async {
                self.countryLabel.text = newValue.country
                self.pressureLabel.text = newValue.pressure
                self.humidityLabel.text = newValue.humidity
                self.temperatureLabel.text = newValue.temperature
                self.apparentTemperatureLabel.text = newValue.apparentTemperature
                self.weatherIcon.image = newValue.weatherIcon
                
                self.activityIndicator.stopAnimating()
                self.refreshButton.isEnabled = true
                
                self.particlesView.viewModel.value = WeatherIcons(rawValue: (newValue.model.value?.weatherIcon)!.rawValue)
            }
        }
    }
    
    @objc private func refreshData() {
        ViewModel.initSelf { [unowned self] (result) in
            DispatchQueue.main.async {
                self.viewModel.value = result
                self.modelsBinding()
                self.timer.invalidate()
            }
        }
    }
    
    @objc private func alertDeniedAccess() {
        DispatchQueue.main.async {
            let actionController = UIAlertController(title: "Access to Location error", message: "Access to your location denied. Please verify in Options -> Privacy -> Locate services -> WeatherApp", preferredStyle: .alert)
            let OkAction = UIAlertAction(title: "Ok", style: .default)
            actionController.addAction(OkAction)
            self.present(actionController, animated: true)
        }
    }
    
    private func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 20.0, repeats: false, block: { [unowned self] _ in
            DispatchQueue.main.async {
                let actionController = UIAlertController(title: "Network error", message: "App can't reach server", preferredStyle: .alert)
                let OkAction = UIAlertAction(title: "Ok", style: .default)
                actionController.addAction(OkAction)
                self.present(actionController, animated: true, completion: { [unowned self] in
                    self.refreshButton.isEnabled = true
                })
            }
        })
    }
}

extension UIButton {
    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(.pi * 2.0)
        rotateAnimation.duration = duration
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
