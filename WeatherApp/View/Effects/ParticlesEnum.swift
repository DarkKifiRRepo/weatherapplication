//
//  ParticlesEnum.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 22/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation

enum ParticlesEnum {
    case rain
    case snow
    case hail
    case sleet
    case wind
    case other
    
    init(weatherType: String) {
        switch weatherType {
        case "rain": self = .rain
        case "snow": self = .snow
        case "sleet": self = .sleet
        case "wind": self = .wind
        case "hail": self = .hail
        default: self = .other
        }
    }
}

extension ParticlesEnum {
    var particle: String {
        switch self {
        case .rain: return "particle-drop"
        case .snow: return "particle-snowflake"
        case .sleet: return ""
        case .wind: return "particle-leaf"
        case .hail: return "particle-spark"
        default: return "other"
        }
    }
}
