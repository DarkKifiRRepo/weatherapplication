//
//  CurrentWeatherProtocol.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 19/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import UIKit

// response data in US units!

protocol CurrentWeatherModel: class, JSONDecodable {
    
    var country: String { get }
    var weatherIcon: WeatherIcons { get }
    var pressure: Double { get set }
    var humidity: Double { get set }
    var temperature: Double { get set }
    var apparentTemperature: Double { get set }
}

extension CurrentWeatherModel {
    static func updateModel(completionHandler: @escaping (CurrentWeatherModel)->Void) {
        let weatherManager = APIWeatherManager.init(apiKey: Constants().apiKey)
        weatherManager.fetchCurrentWeatherWith(coordinates: LocationManager.instance.coordinates) { (result) in
            switch result {
            case .Success(let currentWeather):
                print("Data collect")
                completionHandler(currentWeather)
            case .Failure(let error as NSError):
                print("Error: \(error.description)")
            }
        }
    }
}

final class CurrentWeather: CurrentWeatherModel, OrginizableData {
    
    var country: String
    var weatherIcon: WeatherIcons
    var pressure: Double
    var humidity: Double
    var temperature: Double
    var apparentTemperature: Double
    
    init(country: String, icon: WeatherIcons, pressure: Double, humidity: Double, temperature: Double, apparentTemperature: Double) {
        self.country = country
        self.humidity = humidity
        self.pressure = pressure
        self.temperature = temperature
        self.apparentTemperature = apparentTemperature
        self.weatherIcon = icon
    }
}

extension CurrentWeather: JSONDecodable {
    convenience init?(JSON: [String : AnyObject]) {
        guard let temperature = JSON["temperature"] as? Double,
            let apparentTemperature = JSON["apparentTemperature"] as? Double,
            let humidity = JSON["humidity"] as? Double,
            let pressure = JSON["pressure"] as? Double,
            let iconString = JSON["icon"] as? String else {
                return nil
        }
        
        let icon = WeatherIcons.init(rawValue: iconString)
        let country = LocationManager.instance.userPlace
        self.init(country: country ?? "Location unknown", icon: icon, pressure: pressure, humidity: humidity, temperature: temperature, apparentTemperature: apparentTemperature)
    }
}
