//
//  ParticleGenerator.swift
//  WeatherApp
//
//  Created by Александр Евсеев on 22/03/2019.
//  Copyright © 2019 com.Evseev. All rights reserved.
//

import Foundation
import SpriteKit

protocol ParticleSceneGenerator: class { }
// need more config weather!
extension ParticleSceneGenerator {
    static func emitterInit(emitter: String,position pos: CGPoint, positionRange pRange: CGVector, texture: String, lifeTime time: CGFloat, birthRate rate: CGFloat, speed: CGFloat) -> SKEmitterNode? {
        
        if let particlesEmitter = SKEmitterNode(fileNamed: emitter) {
            particlesEmitter.position = pos
            particlesEmitter.particlePositionRange = pRange
            particlesEmitter.particleTexture = SKTexture(imageNamed: texture)
            particlesEmitter.particleLifetime = time
            particlesEmitter.particleSpeed = speed
            particlesEmitter.particleBirthRate = rate
            return particlesEmitter
        }
        return nil
    }
    
    func generate(with weather: WeatherIcons, toView: SKView) -> [SKEmitterNode?] {
        let scene = toView
        switch weather {
        case .Snow:
            let particles = Self.emitterInit(
                    emitter: "ParticleScene.sks",
                    position: CGPoint(x: scene.frame.size.width / 2, y: scene.frame.size.height),
                    positionRange: CGVector(dx: toView.bounds.size.width, dy: 0),
                    texture: "particle-snowflake", lifeTime: 3, birthRate: 200, speed: 30)
            particles?.name = WeatherIcons.Snow.rawValue
            return [particles]
        case .Rain:
            let particles = Self.emitterInit(
                    emitter: "ParticleScene.sks",
                    position: CGPoint(x: scene.frame.size.width / 2, y: scene.frame.size.height),
                    positionRange: CGVector(dx: toView.bounds.size.width, dy: 0),
                    texture: "particle-drop", lifeTime: 1.5, birthRate: 300, speed: 300)
            particles?.name = WeatherIcons.Rain.rawValue
            return [particles]
        case .Hail:
            let particles = Self.emitterInit(
                    emitter: "ParticleScene.sks",
                    position: CGPoint(x: scene.frame.size.width / 2, y: scene.frame.size.height),
                    positionRange: CGVector(dx: toView.bounds.size.width, dy: 0),
                    texture: "particle-spark", lifeTime: 1, birthRate: 200, speed: 500)
            particles?.name = WeatherIcons.Hail.rawValue
            return [particles]
        case .Sleet:
            let particlesDrop = Self.emitterInit(
                    emitter: "ParticleScene.sks",
                    position: CGPoint(x: scene.frame.size.width / 2, y: scene.frame.size.height),
                    positionRange: CGVector(dx: toView.bounds.size.width, dy: 0),
                    texture: "particle-drop", lifeTime: 1.5, birthRate: 150, speed: 300)
            let particlesSnowlake = Self.emitterInit(
                    emitter: "ParticleScene.sks",
                    position: CGPoint(x: scene.frame.size.width / 2, y: scene.frame.size.height),
                    positionRange: CGVector(dx: toView.bounds.size.width, dy: 0),
                    texture: "particle-snowflake", lifeTime: 3, birthRate: 150, speed: 30)
            particlesDrop?.name = WeatherIcons.Rain.rawValue
            particlesSnowlake?.name = WeatherIcons.Snow.rawValue
            return [particlesDrop, particlesSnowlake]
        case .Wind:
            let particles = Self.emitterInit(
                    emitter: "ParticleScene.sks",
                    position: CGPoint(x: 0, y: scene.frame.size.height / 2),
                    positionRange: CGVector(dx: 0, dy: toView.bounds.size.height),
                    texture: "particle-leaf", lifeTime: 1.5, birthRate: 150, speed: 200)
            particles?.emissionAngle = 0
            particles?.particleSpeedRange = 150
            particles?.yAcceleration = 1
            particles?.particleRotation = 0
            particles?.particleRotationRange = 360
            particles?.particleRotationSpeed = 10
            particles?.particleScaleRange = 0.1
            particles?.name = WeatherIcons.Wind.rawValue
            return [particles]
        default:
            return []
        }
    }
}
